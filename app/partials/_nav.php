<nav class="navbar fixed-top">
    <div class="navigation-bar-left d-flex align-items-center">

        <div class="main-logo-wrapper">
            <a class="main-logo" href="">
                <!--            <span class="logo d-none d-xs-block"></span>-->
                <span class="logo-mobile d-block d-xs-none">
                    <?php include('img/logo.svg'); ?>
                </span>
            </a>
        </div>

    </div>

    <div class="navigation-bar-right">
        <div class="menu-item-icons d-inline-block align-middle">

            <a class="btn btn-sm btn-outline-primary mr-2 d-none d-md-inline-block mb-2" href="/">Remove?</a>

            <div class="position-relative d-none d-sm-inline-block">
                <a href="#" class="hamburger-button d-none d-md-block">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                        <rect x="0.5" y="0.5" width="25" height="1"/>
                        <rect x="0.5" y="7.5" width="25" height="1"/>
                        <rect x="0.5" y="15.5" width="25" height="1"/>
                    </svg>
                </a>

                <a href="#" class="hamburger-button-mobile d-xs-block d-sm-block d-md-none">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26 17">
                        <rect x="0.5" y="0.5" width="25" height="1"/>
                        <rect x="0.5" y="7.5" width="25" height="1"/>
                        <rect x="0.5" y="15.5" width="25" height="1"/>
                    </svg>
                </a>
            </div>

            <div class="position-relative d-none d-sm-inline-block">

                <button class="menu-item-icon btn btn-empty" type="button" id="" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <i class="simple-icon-grid"></i>
                </button>

                <div class="dropdown-menu mt-3 position-absolute" id="menu-icon-dropdown">
                    <a href="#" class="menu-list-item">
                        <i class="HERE-ICON-CLASS d-block"></i>
                        <span>Settings</span>
                    </a>

                    <a href="#" class="menu-list-item">
                        <i class="HERE-ICON-CLASS d-block"></i>
                        <span>agents</span>
                    </a>

                    <a href="#" class="menu-list-item">
                        <i class="HERE-ICON-CLASS d-block"></i>
                        <span>Components</span>
                    </a>

                    <a href="#" class="menu-list-item">
                        <i class="HERE-ICON-CLASS d-block"></i>
                        <span>Profits</span>
                    </a>

                    <a href="#" class="menu-list-item">
                        <i class="HERE-ICON-CLASS d-block"></i>
                        <span>Surveys</span>
                    </a>

                    <a href="#" class="menu-list-item">
                        <i class="HERE-ICON-CLASS d-block"></i>
                        <span>Tasks</span>
                    </a>
                </div>
            </div>

            <div class="position-relative d-inline-block">
                <button class="menu-item-icon btn btn-empty" type="button" id="notification-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                    <i class="BELL ICON"></i>
                    <span class="notification-count-int">3</span>
                </button>
                <div class="dropdown-menu mt-3 scroll position-absolute" id="notification-dropdown">

                    <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                        <a href="#">
                            <img src="img/people.jpeg" class="img-thumbnail listing-img-thumbnail x-small border-0 rounded-circle"/>
                        </a>
                        <div class="pl-3 pr-2">
                            <a href="#">
                                <p class="font-medium mb-1">Joisse Kaycee just sent a new comment!</p>
                                <p class="text-marking--dumb mb-0 text-marking--petit">09.04.2018 - 12:45</p>
                            </a>
                        </div>
                    </div>

                    <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                        <a href="#">
                            <img src="img/notification-thumb.jpg" alt="Notification Image" class="img-thumbnail listing-img-thumbnail x-small border-0 rounded-circle"/>
                        </a>
                        <div class="pl-3 pr-2">
                            <a href="#">
                                <p class="font-medium mb-1">1 item is out of stock!</p>
                                <p class="text-marking--dumb text-marking--petit mb-0">09.04.2018 - 12:45</p>
                            </a>
                        </div>
                    </div>


                    <div class="d-flex flex-row mb-3 pb-3 border-bottom">
                        <a href="#">
                            <img src="img/notification-thumb-2.jpg" class="img-thumbnail listing-img-thumbnail x-small border-0 rounded-circle"/>
                        </a>
                        <div class="pl-3 pr-2">
                            <a href="#">
                                <p class="font-medium mb-1">New order received! It is total $147,20.</p>
                                <p class="text-marking--dumb text-marking--petit mb-0">09.04.2018 - 12:45</p>
                            </a>
                        </div>
                    </div>

                    <div class="d-flex  mb-3 pb-3 ">
                        <a href="#">
                            <img src="img/notification-thumb-3.jpg" class="img-thumbnail listing-img-thumbnail x-small border-0 rounded-circle"/>
                        </a>
                        <div class="pl-3 pr-2">
                            <a href="#">
                                <p class="font-medium mb-1">3 items just added to wish list by a agent!</p>
                                <p class="text-marking--dumb text-marking--petit mb-0">09.04.2018 - 12:45</p>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="agent d-inline-block">
            <button class="btn btn-empty p-0" type="button" data-toggle="dropdown" aria-haspopup="true"
                    aria-expanded="false">
                <span class="name">Erika Berger</span>
                <span>
                        <img alt="Profile Picture" src="img/profile__1.jpg"/>
                    </span>
            </button>

            <div class="dropdown-menu mt-3">
                <a class="dropdown-item" href="#">Account</a>
                <a class="dropdown-item" href="#">Features</a>
                <a class="dropdown-item" href="#">History</a>
                <a class="dropdown-item" href="#">Support</a>
                <a class="dropdown-item" href="#">Sign out</a>
            </div>
        </div>
    </div>
</nav>