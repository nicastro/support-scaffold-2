<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Support</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--    <link rel="stylesheet" href="font/iconsmind/style.css"/>-->
    <!--    <link rel="stylesheet" href="font/simple-line-icons/css/simple-line-icons.css"/>-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!--    <link rel="stylesheet" href="css/vendor/perfect-scrollbar.css"/>-->
    <!--    <link rel="stylesheet" href="css/vendor/jquery.contextMenu.min.css"/>-->
    <link rel="stylesheet" href="styles/css/styles.css"/>
    <link href="styles/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">

</head>

<body class="main-is-hide sub-menu-hidden">

<?php include('partials/_nav.php') ?>
<?php //include('partials/_sidebar_TODO.php');?>

<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1>Layout List</h1>
                    <div class="float-sm-right text-zero">
                        <button type="button" class="btn btn-primary btn-lg top-right-button mr-1">ADD NEW</button>

                        <div class="btn-group ">
                            <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                    <input type="checkbox" class="custom-control-input" id="checkAll">
                                    <span class="custom-control-label"></span>
                                </label>
                            </div>
                            <button type="button" class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split pl-2 pr-2"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                            </div>
                        </div>
                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Library</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                </div>

                <div class="mb-2">
                    <a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
                       role="button" aria-expanded="true" aria-controls="displayOptions">
                        Display Options
                        <i class="simple-icon-arrow-down align-middle"></i>
                    </a>
                    <div class="collapse d-md-block" id="displayOptions">
                            <span class="mr-3 mb-2 d-inline-block float-md-left">
                                <a href="#" class="mr-2 view-icon active">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19">
                                        <path class="view-icon-svg" d="M17.5,3H.5a.5.5,0,0,1,0-1h17a.5.5,0,0,1,0,1Z"/>
                                        <path class="view-icon-svg" d="M17.5,10H.5a.5.5,0,0,1,0-1h17a.5.5,0,0,1,0,1Z"/>
                                        <path class="view-icon-svg" d="M17.5,17H.5a.5.5,0,0,1,0-1h17a.5.5,0,0,1,0,1Z"/></svg>
                                </a>
                                <a href="#" class="mr-2 view-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19">
                                        <path class="view-icon-svg" d="M17.5,3H6.5a.5.5,0,0,1,0-1h11a.5.5,0,0,1,0,1Z"/>
                                        <path class="view-icon-svg"
                                              d="M3,2V3H1V2H3m.12-1H.88A.87.87,0,0,0,0,1.88V3.12A.87.87,0,0,0,.88,4H3.12A.87.87,0,0,0,4,3.12V1.88A.87.87,0,0,0,3.12,1Z"/>
                                        <path class="view-icon-svg"
                                              d="M3,9v1H1V9H3m.12-1H.88A.87.87,0,0,0,0,8.88v1.24A.87.87,0,0,0,.88,11H3.12A.87.87,0,0,0,4,10.12V8.88A.87.87,0,0,0,3.12,8Z"/>
                                        <path class="view-icon-svg"
                                              d="M3,16v1H1V16H3m.12-1H.88a.87.87,0,0,0-.88.88v1.24A.87.87,0,0,0,.88,18H3.12A.87.87,0,0,0,4,17.12V15.88A.87.87,0,0,0,3.12,15Z"/>
                                        <path class="view-icon-svg" d="M17.5,10H6.5a.5.5,0,0,1,0-1h11a.5.5,0,0,1,0,1Z"/>
                                        <path class="view-icon-svg" d="M17.5,17H6.5a.5.5,0,0,1,0-1h11a.5.5,0,0,1,0,1Z"/></svg>
                                </a>
                                <a href="#" class="mr-2 view-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19">
                                        <path class="view-icon-svg"
                                              d="M7,2V8H1V2H7m.12-1H.88A.87.87,0,0,0,0,1.88V8.12A.87.87,0,0,0,.88,9H7.12A.87.87,0,0,0,8,8.12V1.88A.87.87,0,0,0,7.12,1Z"/>
                                        <path class="view-icon-svg"
                                              d="M17,2V8H11V2h6m.12-1H10.88a.87.87,0,0,0-.88.88V8.12a.87.87,0,0,0,.88.88h6.24A.87.87,0,0,0,18,8.12V1.88A.87.87,0,0,0,17.12,1Z"/>
                                        <path class="view-icon-svg"
                                              d="M7,12v6H1V12H7m.12-1H.88a.87.87,0,0,0-.88.88v6.24A.87.87,0,0,0,.88,19H7.12A.87.87,0,0,0,8,18.12V11.88A.87.87,0,0,0,7.12,11Z"/>
                                        <path class="view-icon-svg"
                                              d="M17,12v6H11V12h6m.12-1H10.88a.87.87,0,0,0-.88.88v6.24a.87.87,0,0,0,.88.88h6.24a.87.87,0,0,0,.88-.88V11.88a.87.87,0,0,0-.88-.88Z"/></svg>
                                </a>
                            </span>
                        <div class="d-block d-md-inline-block">
                            <div class="btn-group float-md-left mr-1 mb-1">
                                <button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Order By
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                            <div class="search-sm d-inline-block float-md-left mr-1 mb-1 align-top">
                                <input placeholder="Search...">
                            </div>
                        </div>
                        <div class="float-md-right">
                            <span class="text-marking--dumb text-marking--petit">Displaying 1-10 of 210 items </span>
                            <button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                20
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">10</a>
                                <a class="dropdown-item active" href="#">20</a>
                                <a class="dropdown-item" href="#">30</a>
                                <a class="dropdown-item" href="#">50</a>
                                <a class="dropdown-item" href="#">100</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row customer-view">

            <div class="col-12 col-sm-4 col-md-3 mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <!-- <h5 class="mb-4">Form Grid</h5>-->

                        <div style="text-align: center" class="*">
                            <span style="font-size: 100px;" class="oi oi-person"></span>
                        </div>
                        <div class="row">
                            <div style="text-align: center" class=" col-12 value-group">
                                <a href="/aa">Vymo AG</a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-8 col-md-8 col-lg-6  mb-4">
                <div class="card mb-4">
                    <div class="card-body">
                        <h5 class="mb-4">Form Grid</h5>

                        <!--                        <form>-->
                        <div class="row">
                            <div class="col-6 value-group">
                                <div class="">
                                    <span>Anrede</span>
                                    <span>Herr</span>
                                </div>
                            </div>
                            <div class="col-6 value-group">

                                <div class="">
                                    <span>Titel</span>
                                    <span>Dr.</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6 value-group">
                                <div class="">
                                    <span>Vorname</span>
                                    <span>Daniele</span>
                                </div>
                            </div>
                            <div class="col-6 value-group">
                                <div class="">
                                    <span>Nachname</span>
                                    <span>Nicastro</span>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 value-group">
                                <div class="">
                                    <span>Email</span>
                                    <span>daniele@nicastro.io</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-6 value-group">
                                <div class="">
                                    <span>Telefon</span>
                                    <span>44475444</span>
                                </div>
                            </div>

                            <div class="col-6 value-group">
                                <div class="">
                                    <span>Mobile</span>
                                    <span>5478754578</span>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 value-group">
                                <div class="">
                                    <span>Funktion</span>
                                    <span>Applikationsmanager</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 value-group">
                                <div class="">
                                    <span>Abteilung/Gebäude/Stockwerk</span>
                                    <span>Gebäude A, 4. Stockwerk<br/>Eingang links!</span>
                                </div>
                            </div>
                        </div>


                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                            <label class="form-check-label" for="inlineCheckbox1">1</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                            <label class="form-check-label" for="inlineCheckbox2">2</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="option3"
                                   disabled>
                            <label class="form-check-label" for="inlineCheckbox3">3 (disabled)</label>
                        </div>
                        <form>
                            <button type="submit" class="btn btn-primary d-block mt-3">Bearbeiten</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<!--<script src="js/vendor/jquery-3.3.1.min.js"></script>-->
<!--<script src="js/vendor/bootstrap.bundle.min.js"></script>-->
<!--<script src="js/vendor/perfect-scrollbar.min.js"></script>-->
<!--<script src="js/vendor/mousetrap.min.js"></script>-->
<!--<script src="js/vendor/jquery.contextMenu.min.js"></script>-->
<!--<script src="js/dore.script.js"></script>-->
<!--<script src="js/scripts.js"></script>-->
</body>

</html>