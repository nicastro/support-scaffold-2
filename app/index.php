<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Support</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--    <link rel="stylesheet" href="font/iconsmind/style.css"/>-->
    <!--    <link rel="stylesheet" href="font/simple-line-icons/css/simple-line-icons.css"/>-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!--    <link rel="stylesheet" href="css/vendor/perfect-scrollbar.css"/>-->
    <!--    <link rel="stylesheet" href="css/vendor/jquery.contextMenu.min.css"/>-->
    <link rel="stylesheet" href="styles/css/styles.css"/>

    <!--    <link href="styles/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
</head>

<body class="main-is-hide tickets-all-wrapper">

<?php include('partials/_nav.php') ?>
<?php //include('partials/_sidebar_TODO.php');?>

<main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="mb-2">
                    <h1>Layout List</h1>
                    <div class="float-sm-right text-zero">
                        <button type="button" class="btn btn-primary btn-lg top-right-button mr-1">ADD NEW</button>

                        <div class="btn-group ">
                            <div class="btn btn-primary btn-lg pl-4 pr-0 check-button">
                                <label class="custom-control custom-checkbox mb-0 d-inline-block">
                                    <input type="checkbox" class="custom-control-input" id="checkAll">
                                    <span class="custom-control-label"></span>
                                </label>
                            </div>
                            <button type="button" class="btn btn-lg btn-primary dropdown-toggle dropdown-toggle-split pl-2 pr-2"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Action</a>
                                <a class="dropdown-item" href="#">Another action</a>
                            </div>
                        </div>
                    </div>
                    <nav class="breadcrumb-container d-none d-sm-block d-lg-inline-block" aria-label="breadcrumb">
                        <ol class="breadcrumb pt-0">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="#">Library</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Data</li>
                        </ol>
                    </nav>
                </div>

                <div class="mb-2">
                    <a class="btn pt-0 pl-0 d-inline-block d-md-none" data-toggle="collapse" href="#displayOptions"
                       role="button" aria-expanded="true" aria-controls="displayOptions">
                        Display Options
                        <i class="simple-icon-arrow-down align-middle"></i>
                    </a>
                    <div class="collapse d-md-block" id="displayOptions">
                            <span class="mr-3 mb-2 d-inline-block float-md-left">
                                <a href="#" class="mr-2 view-icon active">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19">
                                        <path class="view-icon-svg" d="M17.5,3H.5a.5.5,0,0,1,0-1h17a.5.5,0,0,1,0,1Z"/>
                                        <path class="view-icon-svg" d="M17.5,10H.5a.5.5,0,0,1,0-1h17a.5.5,0,0,1,0,1Z"/>
                                        <path class="view-icon-svg" d="M17.5,17H.5a.5.5,0,0,1,0-1h17a.5.5,0,0,1,0,1Z"/></svg>
                                </a>
                                <a href="#" class="mr-2 view-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19">
                                        <path class="view-icon-svg" d="M17.5,3H6.5a.5.5,0,0,1,0-1h11a.5.5,0,0,1,0,1Z"/>
                                        <path class="view-icon-svg"
                                              d="M3,2V3H1V2H3m.12-1H.88A.87.87,0,0,0,0,1.88V3.12A.87.87,0,0,0,.88,4H3.12A.87.87,0,0,0,4,3.12V1.88A.87.87,0,0,0,3.12,1Z"/>
                                        <path class="view-icon-svg"
                                              d="M3,9v1H1V9H3m.12-1H.88A.87.87,0,0,0,0,8.88v1.24A.87.87,0,0,0,.88,11H3.12A.87.87,0,0,0,4,10.12V8.88A.87.87,0,0,0,3.12,8Z"/>
                                        <path class="view-icon-svg"
                                              d="M3,16v1H1V16H3m.12-1H.88a.87.87,0,0,0-.88.88v1.24A.87.87,0,0,0,.88,18H3.12A.87.87,0,0,0,4,17.12V15.88A.87.87,0,0,0,3.12,15Z"/>
                                        <path class="view-icon-svg" d="M17.5,10H6.5a.5.5,0,0,1,0-1h11a.5.5,0,0,1,0,1Z"/>
                                        <path class="view-icon-svg" d="M17.5,17H6.5a.5.5,0,0,1,0-1h11a.5.5,0,0,1,0,1Z"/></svg>
                                </a>
                                <a href="#" class="mr-2 view-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19 19">
                                        <path class="view-icon-svg"
                                              d="M7,2V8H1V2H7m.12-1H.88A.87.87,0,0,0,0,1.88V8.12A.87.87,0,0,0,.88,9H7.12A.87.87,0,0,0,8,8.12V1.88A.87.87,0,0,0,7.12,1Z"/>
                                        <path class="view-icon-svg"
                                              d="M17,2V8H11V2h6m.12-1H10.88a.87.87,0,0,0-.88.88V8.12a.87.87,0,0,0,.88.88h6.24A.87.87,0,0,0,18,8.12V1.88A.87.87,0,0,0,17.12,1Z"/>
                                        <path class="view-icon-svg"
                                              d="M7,12v6H1V12H7m.12-1H.88a.87.87,0,0,0-.88.88v6.24A.87.87,0,0,0,.88,19H7.12A.87.87,0,0,0,8,18.12V11.88A.87.87,0,0,0,7.12,11Z"/>
                                        <path class="view-icon-svg"
                                              d="M17,12v6H11V12h6m.12-1H10.88a.87.87,0,0,0-.88.88v6.24a.87.87,0,0,0,.88.88h6.24a.87.87,0,0,0,.88-.88V11.88a.87.87,0,0,0-.88-.88Z"/></svg>
                                </a>
                            </span>
                        <div class="d-block d-md-inline-block">
                            <div class="btn-group float-md-left mr-1 mb-1">
                                <button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Order By
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                </div>
                            </div>
                            <div class="search-sm d-inline-block float-md-left mr-1 mb-1 align-top">
                                <input placeholder="Search...">
                            </div>
                        </div>
                        <div class="float-md-right">
                            <span class="text-marking--dumb text-marking--petit">Displaying 1-10 of 210 items </span>
                            <button class="btn btn-outline-dark btn-xs dropdown-toggle" type="button" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                20
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">10</a>
                                <a class="dropdown-item active" href="#">20</a>
                                <a class="dropdown-item" href="#">30</a>
                                <a class="dropdown-item" href="#">50</a>
                                <a class="dropdown-item" href="#">100</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="separator mb-5"></div>
            </div>
        </div>

        <div class="row ticket-list">
            <div class="col-12" data-check-all="checkAll">


                <div class="card d-flex flex-row mb-3">
                    <div class="d-flex flex-grow-1 min-width-zero">
                        <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">

                            <div class="title-date list-item-heading mb-1 truncate w-40 w-xs-100">
                                <a href="">#523 - Filteransicht benötigt Filteransicht</a>
                                <span class="date text-marking--dumb text-marking--petit">am 27.10.2018 - 13.36 Uhr</span>
                            </div>

                            <div class="customer-company-wrapper mb-1 w-20 w-xs-100">
                                <div class="customer d-flex">
                                    <img class="selection" src="img/dani.jpg" alt="">
                                    <div class="label-select-wrapper d-flex">
                                        <label for="customer" class="text-marking--dumb">Kunde</label>
                                        <select class="with-img selection-item" name="customer">
                                            <option value="dani">Daniele Nicastro</option>
                                            <option value="nora">Nora Meier</option>
                                            <option value="maya">Maya Meier</option>
                                            <option value="hans">Marc Neugass</option>
                                        </select>
                                        <span class="fa fa-chevron-down"><i class="hidden">customer</i></span>
                                    </div>

                                </div>
                                <span class="company">Vymo AG</span>

                                <!--                                <div class="company">-->
                                <!--                                    <img class="selection" src="img/dani.jpg" alt="">-->
                                <!--                                                                        <span class="text-marking--dumb">Firma</span>-->
                                <!--                                    <span>Vymo AG</span>-->
                                <!--                                </div>-->
                            </div>

                            <div class="agent-wrapper mb-1 w-20 w-xs-100">
                                <div class="agent d-flex">
                                    <img class="selection" src="img/dani.jpg" alt="">
                                    <div class="label-select-wrapper d-flex">
                                        <label for="agent" class="text-marking--dumb">Agent</label>
                                        <select class="with-img selection-item" name="agent">
                                            <option value="dani">Daniele Nicastro</option>
                                            <option value="nora">Nora Meier</option>
                                            <option value="maya">Maya Meier</option>
                                            <option value="hans">Marc Neugass</option>
                                        </select>
                                        <span class="fa fa-chevron-down"><i class="hidden">agent</i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="status-priority-wrapper mb-1 w-15 w-xs-100">
                                <div class="status d-flex">
                                    <div class="label-select-wrapper d-flex">
                                        <label for="status" class="text-marking--dumb">Status</label>
                                        <select class="selection-item normal" name="status">
                                            <option value="work">in Bearbeitung</option>
                                            <option value="feedback">Feedback</option>
                                            <option value="solved">Solved</option>
                                            <option value="close">Closed</option>
                                        </select>
                                        <span class="fa fa-chevron-down"><i class="hidden">status</i></span>
                                    </div>
                                </div>

                                <div class="priority d-flex">
                                    <div class="label-select-wrapper d-flex">
                                        <label for="priority" class="text-marking--dumb">Priorität</label>
                                        <select class="selection-item normal" name="priority">
                                            <option value="low">Tief</option>
                                            <option value="normal">Normal</option>
                                            <option value="contract">Vertrag</option>
                                            <option style="color:red" value="high">Dringend</option>
                                        </select>
                                        <span class="fa fa-chevron-down"><i class="hidden">priority</i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="more-metadata-wrapper mb-1 w-5 w-xs-100">
                                <span class="fa fa-phone"></span>
                                <span class="fa fa-question"></span>
                                <span class="fa fa-tag"></span>
                            </div>


                        </div>

                        <!--                        <div class="custom-control custom-checkbox pl-1 align-self-center pr-4">-->
                        <!--                            <label class="custom-control custom-checkbox mb-0">-->
                        <!--                                <input type="checkbox" class="custom-control-input">-->
                        <!--                                <span class="custom-control-label"></span>-->
                        <!--                            </label>-->
                        <!--                        </div>-->
                    </div>
                </div>

                <div class="card d-flex flex-row mb-3">
                    <div class="d-flex flex-grow-1 min-width-zero">
                        <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">

                            <div class="title-date list-item-heading mb-1 truncate w-40 w-xs-100">
                                <a href="">#523 - Filteransicht benötigt Filteransicht</a>
                                <span class="date text-marking--dumb text-marking--petit">am 27.10.2018 - 13.36 Uhr</span>
                            </div>

                            <div class="customer-company-wrapper mb-1 w-20 w-xs-100">
                                <div class="customer d-flex">
                                    <img class="selection" src="img/dani.jpg" alt="">
                                    <div class="label-select-wrapper d-flex">
                                        <label for="customer" class="text-marking--dumb">Kunde</label>
                                        <select class="with-img selection-item" name="customer">
                                            <option value="dani">Daniele Nicastro</option>
                                            <option value="nora">Nora Meier</option>
                                            <option value="maya">Maya Meier</option>
                                            <option value="hans">Marc Neugass</option>
                                        </select>
                                        <span class="fa fa-chevron-down"><i class="hidden">customer</i></span>
                                    </div>

                                </div>
                                <span class="company">Vymo AG</span>

                                <!--                                <div class="company">-->
                                <!--                                    <img class="selection" src="img/dani.jpg" alt="">-->
                                <!--                                                                        <span class="text-marking--dumb">Firma</span>-->
                                <!--                                    <span>Vymo AG</span>-->
                                <!--                                </div>-->
                            </div>

                            <div class="agent-wrapper mb-1 w-20 w-xs-100">
                                <div class="agent d-flex">
                                    <img class="selection" src="img/dani.jpg" alt="">
                                    <div class="label-select-wrapper d-flex">
                                        <label for="agent" class="text-marking--dumb">Agent</label>
                                        <select class="with-img selection-item" name="agent">
                                            <option value="dani">Daniele Nicastro</option>
                                            <option value="nora">Nora Meier</option>
                                            <option value="maya">Maya Meier</option>
                                            <option value="hans">Marc Neugass</option>
                                        </select>
                                        <span class="fa fa-chevron-down"><i class="hidden">agent</i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="status-priority-wrapper mb-1 w-15 w-xs-100">
                                <div class="status d-flex">
                                    <div class="label-select-wrapper d-flex">
                                        <label for="status" class="text-marking--dumb">Status</label>
                                        <select class="selection-item normal" name="status">
                                            <option value="work">in Bearbeitung</option>
                                            <option value="feedback">Feedback</option>
                                            <option value="solved">Solved</option>
                                            <option value="close">Closed</option>
                                        </select>
                                        <span class="fa fa-chevron-down"><i class="hidden">status</i></span>
                                    </div>
                                </div>

                                <div class="priority d-flex">
                                    <div class="label-select-wrapper d-flex">
                                        <label for="priority" class="text-marking--dumb">Priorität</label>
                                        <select class="selection-item normal" name="priority">
                                            <option value="low">Tief</option>
                                            <option value="normal">Normal</option>
                                            <option value="contract">Vertrag</option>
                                            <option style="color:red" value="high">Dringend</option>
                                        </select>
                                        <span class="fa fa-chevron-down"><i class="hidden">priority</i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="more-metadata-wrapper mb-1 w-5 w-xs-100">
                                <span class="fa fa-phone"></span>
                                <span class="fa fa-question"></span>
                                <span class="fa fa-tag"></span>
                            </div>


                        </div>

                        <!--                        <div class="custom-control custom-checkbox pl-1 align-self-center pr-4">-->
                        <!--                            <label class="custom-control custom-checkbox mb-0">-->
                        <!--                                <input type="checkbox" class="custom-control-input">-->
                        <!--                                <span class="custom-control-label"></span>-->
                        <!--                            </label>-->
                        <!--                        </div>-->
                    </div>
                </div>


                <div class="card d-flex flex-row mb-3">
                    <div class="d-flex flex-grow-1 min-width-zero">
                        <div class="card-body align-self-center d-flex flex-column flex-md-row justify-content-between min-width-zero align-items-md-center">
                            <a class="list-item-heading mb-1 truncate w-40 w-xs-100" href="Layouts.Details.html">
                                Goose Breast
                            </a>
                            <p class="mb-1 text-marking--dumb text-marking--petit w-15 w-xs-100">Cupcakes</p>
                            <p class="mb-1 text-marking--dumb text-marking--petit w-15 w-xs-100">27.02.2018</p>
                            <div class="w-15 w-xs-100">
                                <span class="badge badge-pill badge-secondary">ON HOLD</span>
                            </div>
                        </div>

                        <div class="custom-control custom-checkbox pl-1 align-self-center pr-4">
                            <label class="custom-control custom-checkbox mb-0">
                                <input type="checkbox" class="custom-control-input">
                                <span class="custom-control-label"></span>
                            </label>
                        </div>
                    </div>
                </div>


                <nav class="mt-4 mb-3">
                    <ul class="pagination justify-content-center mb-0">
                        <li class="page-item ">
                            <a class="page-link first" href="#">
                                <i class="simple-icon-control-start"></i>
                            </a>
                        </li>
                        <li class="page-item ">
                            <a class="page-link prev" href="#">
                                <i class="simple-icon-arrow-left"></i>
                            </a>
                        </li>
                        <li class="page-item active">
                            <a class="page-link" href="#">1</a>
                        </li>
                        <li class="page-item ">
                            <a class="page-link" href="#">2</a>
                        </li>
                        <li class="page-item">
                            <a class="page-link" href="#">3</a>
                        </li>
                        <li class="page-item ">
                            <a class="page-link next" href="#" aria-label="Next">
                                <i class="simple-icon-arrow-right"></i>
                            </a>
                        </li>
                        <li class="page-item ">
                            <a class="page-link last" href="#">
                                <i class="simple-icon-control-end"></i>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</main>

<!--<script src="js/vendor/jquery-3.3.1.min.js"></script>-->
<!--<script src="js/vendor/bootstrap.bundle.min.js"></script>-->
<!--<script src="js/vendor/perfect-scrollbar.min.js"></script>-->
<!--<script src="js/vendor/mousetrap.min.js"></script>-->
<!--<script src="js/vendor/jquery.contextMenu.min.js"></script>-->
<!--<script src="js/dore.script.js"></script>-->
<!--<script src="js/scripts.js"></script>-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k"
        crossorigin="anonymous"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script type="text/javascript">

    $('document').ready(function () {

        $(window).on("resize", function () {
            // Set .right's width to the window width minus 480 pixels
            var metaDivHeight = $('.ticket-meta').height();
            var ticketFirstDivHeight = $('.ticket-info:first-child .card').height();
            // Upper than .col-md- adjust div, otherwise set to 0 for default bootstrap behavior
            if ($(window).width() > 991) {
                $(".ticket-info:not(:first-child)").css('top', ticketFirstDivHeight - metaDivHeight)
                $(".ticket-info:nth-child(3)").css('top', parseInt($(".ticket-info:nth-child(3)").css('top')) + 5)
            } else {
                $(".ticket-info:not(:first-child)").css('top', 0)
            }
        }).resize();

        $('select.with-img').select2({
            theme: "bootstrap",
            templateResult: formatStateImg,
        });

        $('select.with-icon').select2({
            theme: "bootstrap",
            templateResult: formatResultIcon,
            templateSelection: formatSelectionIcon,
        });

        $('select.tags').select2({
            width: "100%",
            // maximumSelectionSize: 6,

            // templateResult: formatResultIcon,
            // templateSelection: formatSelectionIcon,
        });

        $('select.normal').select2({
            theme: "bootstrap",

        });

        $('select.with-img').on('select2:select', function (e) {
            $(this).parent().prev().attr('src', 'img/' + $(this).val() + '.jpg');
        });

        function formatStateImg(state) {
            if (!state.id)
                return state.text;

            var baseUrl = "./img";
            return $(
                '<span><img src="' + baseUrl + '/' + state.element.value + '.jpg" class="img-flag" />' + state.text + '</span>'
            );
        };

        function formatResultIcon(state) {
            if (!state.id)
                return state.text;

            var icon = state.element.value;
            if (state.element.value === 'email') {
                icon = 'envelope';
            }
            if (state.element.value === 'from_customer') {
                icon = 'user-circle';
            }
            if (state.element.value === 'extension') {
                icon = 'puzzle-piece';
            }
            return $(
                '<div class="format-selection-icon--wrapper"><span class="fa fa-' + icon + '"></span><span>' + state.text + '</span></div>'
            );
            // return $state;
        };

        function formatSelectionIcon(state) {
            if (!state.id)
                return state.text;

            var icon = state.element.value;
            if (state.element.value === 'email') {
                icon = 'envelope';
            }
            if (state.element.value === 'from_customer') {
                icon = 'user-circle';
            }

            if (state.element.value === 'extension') {
                icon = 'puzzle-piece';
            }
            return $(
                '<div class="format-result-icon--wrapper"><span class="fa fa-' + icon + '"></span><span>' + state.text + '</span></div>'
            );
        };

    });
</script>
</body>

</html>